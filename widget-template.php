<?php
/*
 * Template name: custom template
 * */
?>

<?php get_header(); ?>

	<?php if ( is_active_sidebar( 'custom-template' ) ) : ?>
	<section id="custom-page">
		<?php dynamic_sidebar( 'custom-template' ); ?>
	</section>
	<?php endif; ?>

<?php get_footer(); ?>
