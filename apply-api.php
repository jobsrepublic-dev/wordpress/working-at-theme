<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

//include jquery in header for inline script
wp_enqueue_script('jquery');

get_header(); ?>

	<article class="vacature">

		<div class="container">
			<section id="content-header">
				<h1><?php the_title(); ?></h1>
			</section>
			<section id="content">
				<div class="row">
                    <style>
                        #jobsrepublic-application-form .form-control { display: block; }

                        .vacancy-form .doc-upload-container > .file-button {
                            position: relative;
                            width: 100%;
                            margin-bottom: 25px;
                            margin-right: 10px;
                        }

                        .vacancy-form .submit-button{
                            margin-bottom: 60px;
                        }

                        .vacancy-form .form-group.form-group-inline {
                            display: inline-block;
                        }
                        .vacancy-form .form-group label[for] {
                            font-weight: 500;
                        }
                        .vacancy-form .form-group input.form-control {
                            height: 39px;
                            width: 100%;
                        }
                        .form-group.has-error > input {
                            border: 1px solid #a94442;
                        }

                        .has-error > .error-message {
                            color: #a94442;
                        }
                        .vacancy-form .form-group input.form-control.short {
                            width: 75px;
                        }
                        .vacancy-form .file-button .file-placeholder {
                            padding: 25px;
                            border: 1px dashed #dedede;
                            border-radius: 2px;
                            background-color: rgba(20, 186, 213, 0.05);
                        }
                        .vacancy-form .has-error .file-button .file-placeholder {
                            border-color: rgb(235, 204, 209);
                            background-color: rgb(242, 222, 222);
                        }
                        .vacancy-form .file-button .file-placeholder img {
                            margin-right: 10px;
                        }
                        .vacancy-form .file-button .file-placeholder a {
                            text-decoration: underline;
                            cursor: pointer;
                            color: #337ab7;
                        }
                        .vacancy-form .file-button .file-placeholder a:hover {
                            color: #337ab7;
                        }
                        .vacancy-form .file-button .file-placeholder .remove {
                            text-decoration: none;
                            position: relative;
                            z-index: 101;
                        }
                        .vacancy-form .file-button .file-placeholder .remove:hover {
                            text-decoration: underline;
                        }
                        .vacancy-form .file-button input[type='file'] {
                            position: absolute;
                            top: 0;
                            left: 0;
                            width: 100%;
                            height: 100%;
                            opacity: 0;
                            z-index: 100;
                        }

                        .vacancy-form .form-group input[type='file']:hover {
                            cursor: pointer;
                        }
                        .vacancy-form .doc-upload-container > .form-group:hover {
                            /* background-color: rgba(20, 186, 213, 0.15); */
                            cursor: pointer;
                        }

                        .label-doc-upload {
                            font-weight: 500;
                        }
                        .doc-upload-container {
                            display: flex;
                        }
                        @media (max-width: 560px) {
                            .doc-upload-container {
                                display: flex;
                                flex-direction: column;
                            }
                        }
                    </style>
					<div class="col-md-8">
                        <?php
                            $vacancyReference = get_field('reference', get_the_ID());
                            $vacancyUrl = trim(get_field('application_url', get_the_ID()), '"');
                            $vacancy_form = new Vacancy_Forms($vacancyUrl, $vacancyReference);
                            echo $vacancy_form->formHtml;
                        ?>
					</div>
					<div class="col-md-4 widgets-right">
						<?php if (is_active_sidebar('applyform_sidebar')) : ?>
						<section id="vacancy-sidebar">
							<?php dynamic_sidebar('applyform_sidebar'); ?>
						</section>
						<?php endif; ?>
					</div>
				</div>

			</section>

		</div>
	</article>

<?php get_footer(); ?>
