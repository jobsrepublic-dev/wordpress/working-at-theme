<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>


<?php
//single vacancy
if (is_single()) : ?>
<article class="vacature vacature-single" id="post-<?php the_ID(); ?>">
	<div
		data-track-id="vacancy-page"
		data-track-vacancy-id="<?= get_too_id(); ?>">
	</div>
    <div class="sticky-header js-toggle-scroll">
        <div class="container">
            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
			<?php if (has_local_applicationlink()): ?>
				<a href="<?= get_applicationlink(); ?>" class="btn btn-secondary btn-sm">Solliciteer</a>
			<?php else: ?>
            	<a href="<?= get_applicationlink(); ?>" class="btn btn-secondary btn-sm soft-apply">Solliciteer</a>
			<?php endif; ?>
        </div>
    </div>
    <section id="content-header" class="text-center">
        <div class="container">
            <?php the_title('<h1 class="entry-title">', '</h1>'); ?>
            <ul>
                <?php acf_build_vacancy_property_list(); ?>
            </ul>
			<?php if (has_local_applicationlink()): ?>
            	<a href="<?= get_applicationlink(); ?>" class="btn btn-secondary btn-lg js-scroll">Solliciteer</a>
			<?php else: ?>
            	<a href="<?= get_applicationlink(); ?>" class="btn btn-secondary btn-lg js-scroll soft-apply">Solliciteer</a>
			<?php endif; ?>

        </div>
    </section>

    <?php if (is_active_sidebar('vacancy_top')) : ?>
        <section id="vacancy-top">
            <div class="container">
                <?php dynamic_sidebar('vacancy_top'); ?>
            </div>
        </section>
    <?php endif; ?>

	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<section id="content">
					<?php the_content(sprintf(
    __('Continue reading %s', 'twentyfifteen'),
    the_title('<span class="screen-reader-text">', '</span>', false)
                    )); ?>
				</section>

				<section id="vacancy-under">
					<div class="widget widget_apply_button">
						<?php if (has_local_applicationlink()): ?>
							<a href="<?php the_applicationlink(); ?>" class="btn btn-primary btn-lg applyButton" <?php the_target(get_applicationlink()); ?>>Nu solliciteren</a>
						<?php else: ?>
							<a href="<?php the_applicationlink(); ?>" class="btn btn-primary btn-lg applyButton soft-apply" <?php the_target(get_applicationlink()); ?>>Nu solliciteren</a>
						<?php endif; ?>										
					</div>
				</section>
			</div>
		<?php if (is_active_sidebar('vacancy_sidebar')) : ?>
			<div class="col-md-4 widgets-right">
				<?php dynamic_sidebar('vacancy_sidebar'); ?>
			</div>
		<?php endif; ?>
		</div>
	</div>
</article>
<?php
if (is_active_sidebar('vacancy_under')) : ?>
	<div class="container">
		<section id="vacancy-under">
			<?php dynamic_sidebar('vacancy_under'); ?>
		</section>
	</div>
<?php endif; ?>
<?php
//list of vacancies
else : ?>
<article class="vacature vacature-list" id="post-<?php the_ID(); ?>">
	<div class="row">
		<div class="col-md-9">
			<?php the_title(sprintf('<h2><a href="%s" rel="bookmark">', esc_url(get_permalink())), '</a></h2>'); ?>
			<ul>
				<?php acf_build_vacancy_property_list(); ?>
			</ul>
		</div>
		<div class="col-md-3 text-right">
			<a class="btn btn-lg btn-secondary-hover" href="<?php echo esc_url(get_permalink()); ?>">Bekijk vacature</a>
		</div>
	</div>
</article>
<?php endif; ?>
