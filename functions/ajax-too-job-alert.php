<?php

add_action( 'wp_ajax_jobalert', 'sign_in_job_alert' );
add_action( 'wp_ajax_nopriv_jobalert', 'sign_in_job_alert' );

function sign_in_job_alert()
{
    if (!array_key_exists('email', $_REQUEST) || !is_email($_REQUEST['email'])) {
        wp_send_json_error([
            'message' => __('is geen geldig e-mailadres.')
        ]);
    }

    $data = [
        "frequency" => "D",
        "email" => $_REQUEST['email'],
        "active" => 1,
    ];

    $request = new TOOBase();
    $response = $request->getAPIPostTest('jobalert', json_encode($data));

    wp_send_json_success( $response );

}
