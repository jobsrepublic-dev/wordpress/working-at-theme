<?php

if ( ! apply_filters( 'jobsrepublictoo-installed', false ) )
{
    add_action( 'admin_notices', function() {
        echo '<div class="error">
               <p>Jobsrepublic TOO API is niet geïnstalleerd.</p>
            </div>';
    });
}
