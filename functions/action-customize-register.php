<?php
/**
 * Add options to the theme customizer in the backend of Wordpress
 */
add_action( 'customize_register', function( $wp_customize )
{
    //remove some default sections
    $wp_customize->remove_section( 'background_image' );
    $wp_customize->remove_section( 'static_front_page' );

    //remove default colors
    $wp_customize->remove_setting( 'background_color' );

    //add custom logo
    $wp_customize->add_setting( 'jobsrepublic_one_logo' );

    // add own custom headings
    $wp_customize->add_setting( 'jobsrepublic_one_h7' );

    //add our own custom colors
    $wp_customize->add_setting( 'jobsrepublic_one_primary_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_secondary_color' );

    /*$wp_customize->add_setting( 'jobsrepublic_one_homepage_background_color_odd' );*/
    $wp_customize->add_setting( 'jobsrepublic_one_homepage_background_color_even' );

    $wp_customize->add_setting( 'jobsrepublic_one_vacancy_header_background_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_vacancy_title_text_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_vacancy_properties_text_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_vacancy_button_color' );

    $wp_customize->add_setting( 'jobsrepublic_one_text_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_link_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_headertext_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_widget_bg_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_breadcrumb_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_menu_bg' );
    $wp_customize->add_setting( 'jobsrepublic_one_menu_color' );
    $wp_customize->add_setting( 'jobsrepublic_one_menu_bg_active' );
    $wp_customize->add_setting( 'jobsrepublic_one_menu_color_active' );

    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'jobsrepublic_one_logo', array(
            'label'    => __( 'Upload Logo', 'jobsrepublic_one' ),
            'section'  => 'title_tagline',
            'settings' => 'jobsrepublic_one_logo',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_primary_color', array(
            'label'    => __( 'Primary', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_primary_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_secondary_color', array(
            'label'    => __( 'Secondary', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_secondary_color',
    ) ) );

    /*$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_homepage_background_color_odd', array(
        'label'    => __( 'Homepage background 1', 'jobsrepublic_one' ),
        'section'  => 'colors',
        'settings' => 'jobsrepublic_one_homepage_background_color_odd',
    ) ) );*/

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_homepage_background_color_even', array(
            'label'    => __( 'Homepage background 2', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_homepage_background_color_even',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_vacancy_header_background_color', array(
            'label'    => __( 'Vacancy header background', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_vacancy_header_background_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_vacancy_title_text_color', array(
            'label'    => __( 'Vacancy title', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_vacancy_title_text_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_vacancy_properties_text_color', array(
            'label'    => __( 'Vacancy properties', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_vacancy_properties_text_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_vacancy_button_color', array(
            'label'    => __( 'Vacancy header button', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_vacancy_button_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_text_color', array(
            'label'    => __( 'Default Text', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_text_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_link_color', array(
            'label'    => __( 'Link', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_link_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_headertext_color', array(
            'label'    => __( 'Header text', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_headertext_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_widget_bg_color', array(
            'label'    => __( 'Vacancy sidebar background', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_widget_bg_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_breadcrumb_color', array(
            'label'    => __( 'Breadcrumbs', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_breadcrumb_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_menu_bg', array(
            'label'    => __( 'Menu background', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_menu_bg',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_menu_active_bg', array(
            'label'    => __( 'Menu background active', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_menu_bg_active',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_menu_color', array(
            'label'    => __( 'Menu textcolor', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_menu_color',
    ) ) );

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'jobsrepublic_one_menu_color_active', array(
            'label'    => __( 'Menu textcolor active', 'jobsrepublic_one' ),
            'section'  => 'colors',
            'settings' => 'jobsrepublic_one_menu_color_active',
    ) ) );

});

function jobsrepublic_one_header_style()
{
    echo '<style type="text/css" id="custom-header-css">';

    if ( get_theme_mod( 'jobsrepublic_one_text_color' ) ) {
        echo '
        body {
            color: '.get_theme_mod( 'jobsrepublic_one_text_color' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_menu_bg' ) ) {
        echo '
        nav.navbar-default {
            background-color: '.get_theme_mod( 'jobsrepublic_one_menu_bg' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_menu_color' ) ) {
        echo '
        .navbar-default .navbar-nav > li > a,
        .navbar-default .navbar-nav > li > a:hover,
        .navbar-default .navbar-nav > li > a:focus {
            color: '.get_theme_mod( 'jobsrepublic_one_menu_color' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_menu_bg_active' )
        || get_theme_mod( 'jobsrepublic_one_menu_color_active' ) ) {

        echo'
        .navbar-default .navbar-nav > .active > a,
        .navbar-default .navbar-nav > .active > a:focus,
        .navbar-default .navbar-nav > .active > a:hover {
            background-color: '.get_theme_mod( 'jobsrepublic_one_menu_bg_active' ).';
            color: '.get_theme_mod( 'jobsrepublic_one_menu_color_active' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_link_color' ) ) {
        echo '
        a, a:hover, a:focus {
            color: '.get_theme_mod( 'jobsrepublic_one_link_color' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_primary_color' ) ) {
        echo '
        h1, h1 > a, h1 > a:hover, h2, h2 > a, h2 > a:hover, .text-primary {
            color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        .btn-primary, .btn-primary:hover, .btn-primary-hover:hover, .label-primary, .bg-primary {
            background-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
            border-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        .btn-primary-hover {
            border-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
            color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        .vacature-list {
            border-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        .widget_follow_us a.fa, .widget_share_vacancy a.fa {
            color: white;
            background-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        footer #footer {
            background-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
        }
        footer #footer .widget_follow_us a.fa, footer #footer .widget_share_vacancy a.fa {
            color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
            background-color: white;
        }
        .sendgrid_mc_button_div input {
            background-color: '.get_theme_mod( 'jobsrepublic_one_primary_color' ).';
            color: #fff;
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_secondary_color' ) ) {
        echo '
        .text-secondary {
            color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
        }
        .btn-secondary,
        .btn-secondary-hover:hover,
        .label-secondary,
        .bg-secondary {
            background-color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
            border-color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
        }
        .btn-secondary-hover {
            border-color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
            color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
        }
        .vacature-list {
            border-color: '.get_theme_mod( 'jobsrepublic_one_secondary_color' ).';
        }';
    }

    /*if ( get_theme_mod( 'jobsrepublic_one_homepage_background_color_odd' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_homepage_background_color_odd' );
        echo '
        #custom-page > .widget:nth-child(odd),
        #homepage > .widget:nth-child(odd) {
            background-color: '.$color.';
        }';
    }*/

    if ( get_theme_mod( 'jobsrepublic_one_homepage_background_color_even' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_homepage_background_color_even' );
        echo '
        #custom-page > .widget:nth-child(even),
        #homepage > .widget:nth-child(even) {
            background-color: '.$color.';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_vacancy_header_background_color' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_vacancy_header_background_color' );
        echo '
        .vacature.vacature-single #content-header {
            background-color: '.$color.';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_vacancy_title_text_color' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_vacancy_title_text_color' );
        echo '
        .vacature.vacature-single #content-header .entry-title {
            color: '.$color.';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_vacancy_properties_text_color' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_vacancy_properties_text_color' );
        echo '
        .vacature.vacature-single #content-header ul li {
            color: '.$color.';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_vacancy_button_color' ) ) {
        $color = get_theme_mod( 'jobsrepublic_one_vacancy_button_color' );
        echo '
        .vacature.vacature-single #content-header .btn,
        .vacature.vacature-single .sticky-header .btn {
            background-color: '.$color.';
            border-color: '.$color.';
            color: #fff;
        }
        .vacature.vacature-single #content-header .btn:hover,
        .vacature.vacature-single .sticky-header .btn:hover {
            background-color: '.darken_color($color, 2).';
            border-color: '.darken_color($color, 2).';
        }';
    }

    if ( get_header_image() ) {
        echo '
        #pageheader {
            background:url('.get_header_image().') center;
            -webkit-background-size: cover;
            -moz-background-size:    cover;
            -o-background-size:      cover;
            background-size:         cover;
        }';

        if ( get_theme_mod( 'jobsrepublic_one_headertext_color' ) ) {
            echo '
            #pageheader, #pageheader h1, #pageheader h2 {
                color: '.get_theme_mod( 'jobsrepublic_one_headertext_color' ).';
            }';
        }

        echo '
        body.home nav.navbar-default {
            margin-bottom: 0;
        }';
    } else {
        echo '
        #pageheader {
            display: none;
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_widget_bg_color' ) ) {
        echo '
        body.single-vacancy .widgets-right .widget {
            background-color: '.get_theme_mod( 'jobsrepublic_one_widget_bg_color' ).';
        }';
    }

    if ( get_theme_mod( 'jobsrepublic_one_breadcrumb_color' ) ) {
        echo '
        .gotohome {
            color: '.get_theme_mod( 'jobsrepublic_one_breadcrumb_color' ).';
        }';
    }

    echo '</style>';

}

function darken_color($rgb, $darker=2) {

    $hash = (strpos($rgb, '#') !== false) ? '#' : '';
    $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
    if(strlen($rgb) != 6) return $hash.'000000';
    $darker = ($darker > 1) ? $darker : 1;

    list($R16,$G16,$B16) = str_split($rgb,2);

    $R = sprintf("%02X", floor(hexdec($R16)/$darker));
    $G = sprintf("%02X", floor(hexdec($G16)/$darker));
    $B = sprintf("%02X", floor(hexdec($B16)/$darker));

    return $hash.$R.$G.$B;
}
