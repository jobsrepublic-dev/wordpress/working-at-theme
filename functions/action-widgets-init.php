<?php
/**
 * Register our sidebars and widgetized areas.
 */
add_action( 'widgets_init', function() {

    register_sidebar( array(
        'name'          => 'Homepage',
        'id'            => 'home',
        'before_widget' => '<div class="widget %2$s"><div class="widget-container"><div class="container"><div class="row"><div class="col-md-12">',
        'after_widget'  => '</div></div></div></div></div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Custom Template',
        'id'            => 'custom-template',
        'before_widget' => '<div class="widget %2$s"><div class="widget-container"><div class="container"><div class="row"><div class="col-md-12">',
        'after_widget'  => '</div></div></div></div></div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer',
        'id'            => 'footer',
        'before_widget' => '<div class="widget %2$s"><div class="col-md-6">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Footer Full',
        'id'            => 'footer-full',
        'before_widget' => '<div class="widget %2$s"><div class="col-md-12">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Header',
        'id'            => 'header_right',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<strong>',
        'after_title'   => '</strong>',
    ) );

    register_sidebar( array(
        'name'          => 'Vacature Overzicht Header',
        'id'            => 'vacature_header',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Vacature Overzicht Onder',
        'id'            => 'vacature_footer',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Vacature Sidebar',
        'id'            => 'vacancy_sidebar',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Vacature Boven',
        'id'            => 'vacancy_top',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Vacature Onder',
        'id'            => 'vacancy_under',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name'          => 'Solliciteren Sidebar',
        'id'            => 'applyform_sidebar',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>',
    ) );

});
