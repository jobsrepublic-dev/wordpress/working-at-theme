<?php

add_action( 'current_screen', function() {

    $current_screen = get_current_screen();

    if( $current_screen->id === 'settings_page_jobsrepublic-options' ) {

        $migrate_options = [
            [
                'old_name' => 'too_channel_token',
                'new_name' => 'options_too_channel_token',
                'delete' => false
            ],
            [
                'old_name' => 'too_slug',
                'new_name' => 'options_too_slug',
                'delete' => false
            ]
        ];

        foreach( $migrate_options as $option ) {

            /**
             * continue if already filled
             */
            if ( ! empty( get_option( $option['new_name'] ) ) ) {
                continue;
            }

            if( get_option( $option['old_name'] ) ) {

                update_option( $option['new_name'], get_option( $option['old_name'] ) );

                if($option['delete']) {
                    delete_option( $option['old_name'] );
                }
            }
        }

    }

});
