<?php
class Vacancy_Forms
{
    private $formResult = null;
    public $formHtml = '';

    /**
     * Vacancy_Forms constructor.
     */
    public function __construct($vacancyUrl, $vacancyReference)
    {    
        // No vacancy url and no Too reference set
        if (empty($vacancyUrl) && empty($vacancyReference)) {
            $this->formHtml = "Probeer het nog eens of neem bij aanhoudende problemen contact op met onze supportdesk via <a href=mailto:supportdesk@jobsrepublic.nl>supportdesk@jobsrepublic.nl</a>. (code 4)";
            return;
        }

        // Set vacancy url to default value when not set (only here for backwards compatibility)
        if (empty($vacancyUrl)) {
            $this->vacancyUrl = 'https://vacatures.one/' . $vacancyReference . '/solliciteren?embed=true&channel-token=' . get_option('options_one_channel_token');
        } else {
            $this->vacancyUrl = $vacancyUrl . (strpos($vacancyUrl, '?') !== false ? '&' : '?') .  'embed=true';
        }

        // Error out if the url is not a vacatures.one url
        if (!is_vacatures_one_url($this->vacancyUrl)) {
            $this->formHtml = "Probeer het nog eens of neem bij aanhoudende problemen contact op met onze supportdesk via <a href=mailto:supportdesk@jobsrepublic.nl>supportdesk@jobsrepublic.nl</a>.  (code 3)";
            return;
        }

        if ($this->getForm()) {
            $this->build_html_form();
        }
    }

    private function getForm()
    {
        $this->formResult = wp_remote_get($this->vacancyUrl);

        /**
         * Third error check if is wp error
         */
        if (isset($this->formResult) && is_wp_error($this->formResult)) {
            $this->formHtml = "Probeer het nog eens of neem bij aanhoudende problemen contact op met onze supportdesk via <a href=mailto:supportdesk@jobsrepublic.nl>supportdesk@jobsrepublic.nl</a>. (code 1)";
            return false;
        }


        /**
         * First error check with body response
         */
        if (!isset($this->formResult["response"]["code"]) || $this->formResult["response"]["code"] != '200' && !empty($this->formResult["body"])) {
            $this->formHtml = $this->formResult["body"];
            return false;
        }

        /**
         * Second error check with custom response
         */
        if (!isset($this->formResult["response"]["code"]) || $this->formResult["response"]["code"] != '200') {
            $this->formHtml = "Probeer het nog eens of neem bij aanhoudende problemen contact op met onze supportdesk via <a href=mailto:supportdesk@jobsrepublic.nl>supportdesk@jobsrepublic.nl</a>. (code 2)";
            return false;
        }

        /**
         * Success
         */
        return true;
    }

    private function build_html_form()
    {
        $dom = new DOMDocument();
        @$dom->loadHTML($this->formResult["body"]);
        $form = $dom->getElementsByTagName('body')->item(0);
        $this->formHtml=$dom->saveHtml($form);
        return $this->formHtml;
    }
}
