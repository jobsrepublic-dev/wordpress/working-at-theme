<?php

function acf_build_vacancy_property_list() {

    /**
     * make sure ACF is installed and activated
     */
    if (!function_exists('have_rows')) {
        return;
    }

    // check if the flexible content field has rows of data
    if( have_rows('vacancy_properties', 'options') ):

        // loop through the rows of data
        while ( have_rows('vacancy_properties', 'options') ) : the_row();

            $layout = get_row_layout();
            switch ($layout) {
                case 'show_location':
                    $location = get_the_address_city();
                    if (!empty($location)) {
                        echo "<li>{$location}</li>";
                    }
                    break;
                case 'show_employment_types':
                    $employment_types = get_the_employment_types();
                    if (!empty($employment_types)) {
                        echo "<li>{$employment_types}</li>";
                    }
                    break;
                case 'show_salary':
                    $salary = get_the_salary();
                    if (!empty($salary)) {
                        echo "<li>{$salary}</li>";
                    }
                    break;
                case 'show_work_fields':
                    $work_fields = get_the_work_fields();
                    if (!empty($work_fields)) {
                        echo "<li>{$work_fields}</li>";
                    }
                    break;
                case 'show_education_level':
                    $education_level = get_the_education_levels();
                    if (!empty($education_level)) {
                        echo "<li>{$education_level}</li>";
                    }
                    break;
                case 'show_work_experience':
                    $work_experience = get_the_work_experience();
                    if (!empty($work_experience)) {
                        echo "<li>{$work_experience}</li>";
                    }
                    break;
                case 'show_date_added':
                    $date = get_the_date();
                    if (!empty($date)) {
                        echo "<li>{$date}</li>";
                    }
                    break;
                case 'show_date_closing':
                    $closing_date = get_the_closing_date();
                    if (!empty($closing_date)) {
                        echo "<li>{$closing_date}</li>";
                    }
                    break;

            }

        endwhile;

    endif;

}
