<?php
add_action('init', function () {

    kses_remove_filters();

    add_filter('wp_kses_allowed_html', 'esw_author_cap_filter', 1, 1);

    function esw_author_cap_filter($allowedposttags)
    {

        if (!current_user_can('publish_posts')) {
            return $allowedposttags;
        }

        $allowedposttags['iframe'] = [
            'align'                 => true,
            'width'                 => true,
            'height'                => true,
            'frameborder'           => true,
            'name'                  => true,
            'src'                   => true,
            'id'                    => true,
            'class'                 => true,
            'style'                 => true,
            'scrolling'             => true,
            'marginwidth'           => true,
            'marginheight'          => true,
            'allowfullscreen'       => true,
            'mozallowfullscreen'    => true,
            'webkitallowfullscreen' => true,
        ];

        return $allowedposttags;
    }
});
