<?php

add_action( 'after_setup_theme', function() {

    add_theme_support( 'title-tag' );

    //Geef hier aan welke post formats worden ondersteund, vacancy, company
    add_theme_support( 'post-formats', array(
        'vacancy'
    ) );

    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'custom-background', apply_filters( 'jobsrepublic_one_custom_background_args', array(
        'default-color'      => '#ffffff',
        'default-attachment' => 'fixed',
    ) ) );

    add_theme_support( 'custom-header', apply_filters( 'jobsrepublic_one_custom_header_args', array(
        'default-text-color'     => '#ffffff',
        'width'                  => 1500,
        'height'                 => 859,
        'flex-height'            => true,
        'flex-width'             => true,
        'wp-head-callback'       => 'jobsrepublic_one_header_style',
    ) ) );

    register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );

});
