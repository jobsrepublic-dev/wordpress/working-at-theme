<?php

if( function_exists('acf_add_options_page') ) {

    $option_page = acf_add_options_page(array(
        'page_title'    => 'Jobsrepublic',
        'menu_title'    => 'Jobsrepublic',
        'menu_slug'     => 'jobsrepublic-options',
        'capability'    => 'edit_posts',
        'parent' => 'options-general.php',
        'redirect'  => false
    ));

}
