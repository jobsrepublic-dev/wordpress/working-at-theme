		</main>

		<footer>
			<section id="footer">
				<div class="container">
					<div class="row">
						<?php dynamic_sidebar( 'footer-full' ); ?>
					</div>
					<div class="row">
						<?php dynamic_sidebar( 'footer' ); ?>
					</div>
				</div>
			</section>
			<section id="poweredby">
				<div class="container">Powered by <a href="http://www.jobsrepublic.nl"><img border="0" src="<?php echo get_template_directory_uri(); ?>/dist/img/logo-jobsrepublic.svg" alt="Jobsrepublic"></a></div>
			</section>
		</footer>

        <div id="cookieMessage" class="section-cookienotice" data-component="CookieNotice" data-initialized="true" style="display: none;">
            <p>Als je onze site gebruikt, krijg je van ons <a target="_blank" href="http://cms.jobsrepublic.nl/uploads/pdf/Cookiebeleid.pdf">cookies</a></p>
            <button class="btn"><span class="sr-only">Sluiten</span><i class="fa fa-close"></i></button>
        </div>

		<?php wp_footer(); ?>
	</body>
</html>
