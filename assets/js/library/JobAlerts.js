var JobAlerts = function(url, form) {

  this.url = url;

  this.form = form;

  this.email = undefined;

  this.submit = this.form.querySelector('.jobalertsEmail');

};

JobAlerts.prototype = {

  init: function() {

    if (!this.form) {
      return false;
    }

    this.email = this.form.querySelector('.jobalertsEmail');

    this.submit = this.form.querySelector('input[type="submit"]');

    this.addClickHandler();

  },

  addClickHandler: function() {

    var self = this;

    this.submit.addEventListener('click', function(event) {

      event.preventDefault();

      var error = self.checkInput();

      if (!error) {

        self.email.classList.add('error');

      } else {

        self.email.classList.remove('error');

        self.submitForm();

      }

    }, false);

  },

  submitForm: function() {

    var self = this;

    jQuery.ajax({
      url: this.url,
      method: "POST",
      data: {
        "action": "jobalert",
        "email": this.email.value
      },
      type: 'POST',
      dataType: 'json'
    }).success(function(response) {


      if (!response.success) {

        self.email.classList.add('error');

        self.email.setAttribute('title', response.data);

      } else {

        self.email.classList.remove('error');

        self.makeModal(response.data.email);

      }

    });

  },

  makeModal: function(userEmail) {

    jQuery('#userEmail').html(userEmail);
    jQuery('#my-modal').modal({show: true});
  },

  checkInput: function() {

    var email = new RegExp("^[_a-z0-9-]+(.[a-z0-9-]+)@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,4})$");

    if (this.email.value.length === 0 ||
      !email.test(this.email.value)) {
      return false;
    }

    return true;

  }

};

module.exports = JobAlerts;
