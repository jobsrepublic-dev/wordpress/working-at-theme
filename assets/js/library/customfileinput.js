if(jQuery) (function($){

	$.fn.customFileInput = function(params){
		// default configuration
		var conf = $.extend({
			positionUploadButton: 'bottom'
		}, params);
		// end default configuration
		return this.each(function(){
			var configuration = conf,
				object = $(this),
				object_functions = this,
				multiple = false,
				cloneElem;				
			$.extend(object_functions,{

				init:function(){
					if (object.hasClass("multiple")) {
						multiple = true;
					}
					object_functions.cloneElem();
					object_functions.attachEvent(object);
				},
				cloneElem: function() {
					cloneElem = $('li:first-child', object).clone();		
				},
				attachEvent : function(object) {
					$('li:not(.has-file) input', object).change(object_functions.eventHandler);
				},
				eventHandler : function() {
					var parent = $(this).closest('li');
					
					if(this.value != "") {
						
						$(".file-name", parent).html(object_functions.getFilenames($(this)));
						
						
						parent.addClass("has-file");
					
						// parent.addClass('inactive');
						$(this).attr("id", "").off('change', object_functions.eventHandler);
						$('.file-remove',parent).on('click', object_functions.removeElem);
						
						if(multiple) {
							object_functions.addClone();
						}
					}
				},
				removeElem : function(e) {
					e.preventDefault();
					$(this).closest('li').remove();
					
					if(!multiple) {
						object_functions.addClone();
					}
				},
				addClone : function() {
					if(configuration.positionUploadButton == 'bottom') {
						object.append(cloneElem.clone());
					} else {
						object.prepend(cloneElem.clone());
					}
					object_functions.attachEvent(object);
				},
				getFilenames : function(elem) {
					var fileList = elem[0].files;
					var fileNameArray = [];
					
					if (fileList != undefined && fileList.length > 0) {
						for (var i = 0; i < fileList.length; i++) {			
							fileNameArray.push(fileList[i].name);
						}
						return fileNameArray.join(", ");
					}
				}
			});
			object_functions.init();
		})
	};
})(jQuery);
