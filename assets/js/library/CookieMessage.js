var CookieMessage = function() {
  "use strict";

  this.document = document;

  this.cookieTemplate = document.getElementById('cookieMessage');

};

CookieMessage.prototype = {

  init: function() {
    "use strict";

    var accepted = this.getLocalStorage(),
      element;

    if (!accepted || !JSON.parse(accepted).agreed) {
      element = this.createMessage();
      this.acceptClickHandler(element);
    }

  },

  createMessage: function() {
    "use strict";

    this.cookieTemplate.style.display = 'block';

    return this.cookieTemplate;

  },

  acceptClickHandler: function(element) {
    "use strict";

    var self = this;

    element.querySelector('.btn').addEventListener('click', function(e) {
      self.setLocalStorage();
      self.removeMessage(element);
    });

  },

  getLocalStorage: function() {
    "use strict";

    return window.localStorage.getItem('cookieData');

  },

  setLocalStorage: function() {
    "use strict";

    var data = {
      agreed: true,
      date: Date.now()
    };

    window.localStorage.setItem('cookieData', JSON.stringify(data));

  },

  removeMessage: function(element) {
    "use strict";
    element.style.display = 'none';
  }

};

module.exports = CookieMessage;
