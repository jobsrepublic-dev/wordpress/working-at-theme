var Scroller = function() {
  "use strict";

  this.body = document.body;

  this.elementsToWatch = undefined;

  this.navbar = document.querySelector('.navbar');

  this.stickyHeader = document.querySelector('.js-toggle-scroll');

};

Scroller.prototype.addTestElements = function(className) {
  "use strict";

  var test = document.getElementsByClassName(className);

  if (test.length === 0) {
    return this;
  }

  this.elementsToWatch = test;

  this.addSticky();

  return this;

};

Scroller.prototype.windowScrollHandler = function() {
  "use strict";

  var self = this;

  if (!this.elementsToWatch) {
    return this;
  }

  window.addEventListener('scroll', function() {

    self.addSticky();

  });

  return this;

};

Scroller.prototype.addSticky = function() {
  "use strict";

  var self = this,
    scroller,
    element = this.elementsToWatch[0];

  if (window.matchMedia("(max-width: 767px)").matches) {
    scroller = 0;
  } else {
    scroller = this.navbar.getBoundingClientRect().height + this.navbar.getBoundingClientRect().top;
  }

  if (element.getBoundingClientRect().top + element.getBoundingClientRect().height - scroller < 1) {
    self.toggleActive(true);
  } else {
    self.toggleActive(false);
  }

};

Scroller.prototype.toggleActive = function(add) {
  "use strict";

  if (add) {
    this.stickyHeader.classList.add('is-active');
  } else {
    this.stickyHeader.classList.remove('is-active');
  }

};

export default Scroller;
