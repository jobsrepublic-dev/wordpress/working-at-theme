<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title">Geen vacatures gevonden</h1>
	</header>

	<div class="page-content">

        <?php if (empty($_GET)) :

            if (get_field('message_no_vacancy', 'options')) :?>
                <?= get_field('message_no_vacancy', 'options'); ?>
            <?php else :?>
		        <p>Op dit moment hebben wij geen openstaande vacatures.</p>
            <?php endif; ?>
        <?php else:
            if (get_field('message_no_vacancy_filtering', 'options')) :?>
                <?= get_field('message_no_vacancy_filtering', 'options'); ?>
             <?php else :?>
                <p>Er zijn op dit moment geen vacatures met deze zoekcriteria.
                    Pas je filters aan om vacatures te vinden.</p>
             <?php endif; ?>
        <?php endif; ?>

	</div>
</section>
