<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div id="app" class="vacature_archive">

    <div class="vacature_header">
        <?php
        if (is_active_sidebar('vacature_header')) {
            dynamic_sidebar('vacature_header');
        }
        ?>
    </div>

    <?php the_search_filters(); ?>

	<section id="vacatures">
		<div class="container">

		<?php if ( have_posts() ) : ?>

            <?php
                $site_name = get_field('site_name', 'options');
                $site_title = !empty($site_name) ? ' bij ' . $site_name : '';
            ?>

			<h1 class="page-title">Ontdek jouw nieuwe baan <?= $site_title; ?> <span class="smaller_font">(<?php echo $GLOBALS['wp_query']->found_posts ?> vacatures)</span></h1>

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();

				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'content', get_post_type() );

			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</div>
	</section>

    <div class="vacature_footer">
        <?php
        if (is_active_sidebar('vacature_footer')) {
            dynamic_sidebar('vacature_footer');
        }
        ?>
    </div>

</div>

<?php get_footer();
